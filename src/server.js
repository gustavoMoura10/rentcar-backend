const dotenv = require("dotenv");
dotenv.config();
const express = require("express");
const { initialMiddlewares } = require("./config/middlewares");
const { createRoutes } = require("./config/routes");
const app = express();
(async () => {
  try {
    initialMiddlewares(app);
    createRoutes(app);
  } catch (error) {
    console.log(error);
    const closed = await getConnection().close();
    console.log(`CLOSED:`, closed);
    app.use(function (req, resp, next) {
      resp.status(500).send({
        error: true,
        message: "Internal Server Error",
      });
    });
  } finally {
    app.listen(process.env.PORT, () => {
      console.log("RUNNING ON PORT:", process.env.PORT);
    });
  }
})();
