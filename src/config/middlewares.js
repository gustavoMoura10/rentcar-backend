const express = require("express");
const cors = require("cors");
const morgan = require("morgan");
const jwt = require("jsonwebtoken");
const { findUserByIdQuery } = require("../app/queries/usersQueries");
const Users = require("../app/models/Users");

exports.initialMiddlewares = (app) => {
  app.use(express.json());
  app.use(cors());
  app.use(morgan("dev"));
};

exports.authToken = async (req, resp, next) => {
  let status = 400;
  let errors = [];
  try {
    status = 401;
    const auth = req.headers.Authorization || req.headers.authorization;
    if (!auth) {
      errors.push({
        error: true,
        message: "Missing authorization header",
      });
      throw errors;
    }
    if (!String(auth).includes("Bearer ")) {
      errors.push({
        error: true,
        message: "Wrong authorization format",
      });
      throw errors;
    }
    const token = String(auth).split("Bearer ").pop().trim();
    if (!token) {
      errors.push({
        error: true,
        message: "Wrong authorization format",
      });
      throw errors;
    }
    const payload = jwt.decode(token, process.env.JWT_TOKEN);
    if (!payload) {
      errors.push({
        error: true,
        message: "Wrong payload",
      });
      throw errors;
    }
    status = 403;
    if (payload.exp < (new Date().getTime() + 1) / 1000) {
      errors.push({
        error: true,
        message: "Token expired",
      });
      throw errors;
    }
    const user = await Users.first({ email: payload?.email });
    if (!user) {
      errors.push({
        error: true,
        message: "Coudn't find your user",
      });
      throw errors;
    }
    req.headers["_id"] = payload._id;
    next();
  } catch (error) {
    console.log(error);
    resp.status(status).send(
      status === 500
        ? {
            error: true,
            message: "Internal Server Error",
          }
        : error
    );
  }
};
