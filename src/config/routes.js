const usersRoute = require("../app/routes/usersRoute");
const authRoute = require("../app/routes/authRoute");
const locationsRoute = require("../app/routes/locationsRoute");
const contactsRoute = require("../app/routes/contactsRoute");
const brandsRoute = require("../app/routes/brandsRoute");
const carsRoute = require("../app/routes/carsRoute");

exports.createRoutes = (app) => {
  app.use("/users", usersRoute);
  app.use("/auth", authRoute);
  app.use("/locations", locationsRoute);
  app.use("/contacts", contactsRoute);
  app.use("/brands", brandsRoute);
  app.use("/cars", carsRoute);
};
