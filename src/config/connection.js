const Neode = require("neode");
const path = require("path");

exports.getConnection = () => {
  const neode = new Neode(
    "bolt://localhost:7687",
    process.env.NEO4J_USER,
    process.env.NEO4J_PWD,
    true,
    process.env.NEO4J_DB
  );
  neode.withDirectory(path.join(__dirname, "..", "app", "models"));
  return neode;
};
