const ImageKit = require("imagekit");

exports.imageKit = new ImageKit({
  publicKey: process.env.IMGKIT_PUBLIC_KEY,
  privateKey: process.env.IMGKIT_PRIVATE_KEY,
  urlEndpoint: process.env.IMGKIT_URL_ENDPOINT,
});

exports.bufferImageBase64 = (base64) => {
  const base64Transform = base64.includes("base64")
    ? base64.split(",").pop()
    : base64;
  const buffer = Buffer.from(base64Transform, "base64");
  return buffer;
};
