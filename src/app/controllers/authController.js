const jwt = require("jsonwebtoken");
const Users = require("../models/Users");
const Contacts = require("../models/Contacts");
const Cars = require("../models/Cars");
const Locations = require("../models/Locations");
const Images = require("../models/Images");

const { loginValidate } = require("../validators/authValidator");
const populateUser = async (user) => {
  if (user.location) {
    const location = await (
      await Locations.findById(user.location?._id)
    ).toJson();
    user.location = location;
  }
  if (user.contact) {
    const contact = await (await Contacts.findById(user.contact?._id)).toJson();
    user.contact = contact;
  }
  if (Array.isArray(user.cars) && Array(user.cars).length > 0) {
    user.cars = await Promise.all(
      user.cars.map(async (el) => {
        const car = await (await Cars.findById(el._id)).toJson();

      })
    );
  }
  if (Array.isArray(user.carsRented) && Array(user.carsRented).length > 0) {
    user.carsRented = await Promise.all(
      user.carsRented.map(async (el) => {
        el.node = await (await Cars.findById(el.node?._id)).toJson();
        if (el.node?.brand) {
        }
        return el;
      })
    );
  }
  if(user.image){
    const image = await (await Images.findById(user.image?._id)).toJson();
    user.image = image;
  }
  return user;
};
exports.login = async (req, resp, next) => {
  let status = 400;
  try {
    const validate = await loginValidate(req.body);
    if (validate.length > 0) throw validate;
    status = 500;
    const user = await populateUser(await (await Users.first({ email: req.body?.email })).toJson());
    delete user.password;
    const token = jwt.sign(user, process.env.JWT_TOKEN, {
      expiresIn: "24h",
    });
    status = 200;
    resp.status(status).json({
      ...user,
      token,
    });
  } catch (error) {
    console.log(error);
    resp.status(status).send(
      status === 500
        ? {
          error: true,
          message: "Internal Server Error",
        }
        : error
    );
  }
};

exports.newToken = async (req, resp, next) => {
  let status = 400;
  try {
    status = 500;
    const user = await populateUser(await (await Users.findById(req.headers['_id'])).toJson());
    delete user.password;
    const token = jwt.sign(user, process.env.JWT_TOKEN, {
      expiresIn: "24h",
    });
    status = 200;
    resp.status(status).json({
      ...user,
      token,
    });
  } catch (error) {
    console.log(error);
    resp.status(status).send(
      status === 500
        ? {
          error: true,
          message: "Internal Server Error",
        }
        : error
    );
  }
};
