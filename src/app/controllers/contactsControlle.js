const Contacts = require("../models/Contacts");
const Users = require("../models/Users");
const { createContactValidate } = require("../validators/contactValidator");

exports.createContact = async (req, resp, next) => {
  let status = 400;
  try {
    const result = await createContactValidate(req.body, req.headers);
    if (result.length > 0) throw result;
    status = 500;
    const contact = await Contacts.mergeOn(req.body);
    const user = await Users.findById(req.body?.idUser);
    await user.relateTo(contact, "contactedBy");
    status = 200;
    resp.status(status).json(await contact.toJson());
  } catch (error) {
    console.log(error);
    resp.status(status).send(
      status === 500
        ? [
            {
              error: true,
              message: "Internal Server Error",
            },
          ]
        : error
    );
  }
};
