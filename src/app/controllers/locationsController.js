const Brands = require("../models/Brands");
const Cars = require("../models/Cars");
const Locations = require("../models/Locations");
const Users = require("../models/Users");
//const { createLocationQuery } = require("../queries/locationsQueries");
const {
  createLocationValidate,
  findLocationByIdValidate,
} = require("../validators/locationsValidator");
const populateLocation = async (location) => {
  if (location.user) {
    location.user = await (await Users.findById(location.user._id)).toJson();
    delete location.user.password;
    if (
      Array.isArray(location.user?.cars) &&
      Array(location.user?.cars).length > 0
    ) {
      location.user.cars = await Promise.all(
        location.user.cars.map(async (el) => {
          const car = await (await Cars.findById(el._id)).toJson();
          if (car.brand) {
            car.brand = await (await Brands.findById(car.brand._id)).toJson();
          }
          return car;
        })
      );
    }
  }
  return location;
};
exports.createLocation = async (req, resp, next) => {
  let status = 400;
  try {
    const result = await createLocationValidate(req.body, req.headers);
    if (result.length > 0) throw result;
    status = 500;
    const location = await Locations.mergeOn(req.body);
    const user = await Users.findById(req.body?.idUser);
    await user.relateTo(location, "lives");
    status = 200;
    resp.status(status).json(await location.toJson());
  } catch (error) {
    console.log(error);
    resp.status(status).send(
      status === 500
        ? [
            {
              error: true,
              message: "Internal Server Error",
            },
          ]
        : error
    );
  }
};
exports.findLocationById = async (req, resp, next) => {
  try {
    let status = 400;
    const result = await findLocationByIdValidate(req.params);
    if (result.length > 0) throw result;
    status = 500;
    const location = await (await Locations.findById(req.params?._id)).toJson();
    if (location.user) {
      const user = await (await Users.findById(location.user._id)).toJson();
      location.user = user;
    }
    status = 200;
    resp.status(status).json(location);
  } catch (error) {
    console.log(error);
    resp.status(status).send(
      status === 500
        ? [
            {
              error: true,
              message: "Internal Server Error",
            },
          ]
        : error
    );
  }
};
exports.findAllLocations = async (req, resp, next) => {
  let status = 400;
  try {
    let query = {};
    if (req.query) {
      query = {
        ...req.query,
      };
    }
    status = 500;
    const locations = await Promise.all(
      (await (await Locations.all(query)).toJson()).map((el) =>
        populateLocation(el)
      )
    );
    status = 200;
    resp.status(status).json(locations);
  } catch (error) {
    console.log(error);
    resp.status(status).send(
      status === 500
        ? [
            {
              error: true,
              message: "Internal Server Error",
            },
          ]
        : error
    );
  }
};
