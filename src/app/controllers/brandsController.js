const Brands = require("../models/Brands");
const Cars = require("../models/Cars");
const Images = require("../models/Images");
const Users = require("../models/Users");
const Contacts = require("../models/Contacts");
const { createBrandValidate } = require("../validators/brandsValidator");
const populateBrand = async (brand) => {
  if (Array.isArray(brand.cars) && Array(brand.cars).length > 0) {
    brand.cars = await Promise.all(
      brand.cars.map(async (el) => {
        const car = await (await Cars.findById(el._id)).toJson();
        if (Array.isArray(car.images) && Array(car.images).length > 0) {
          car.images = await Promise.all(
            car.images.map(
              async (elImage) =>
                await (await Images.findById(elImage._id)).toJson()
            )
          );
        }
        if (car.user) {
          car.user = await (await Users.findById(car.user._id)).toJson();
          if (car.user?.contact) {
            car.user.contact = await (
              await Contacts.findById(car.user?.contact._id)
            ).toJson();
          }
          delete car.user.password;
        }
        if (car.rented) {
          car.rented = await (await Users.findById(car.rented._id)).toJson();
        }

        return car;
      })
    );
  }
  return brand;
};
exports.createBrand = async (req, resp, next) => {
  let status = 400;
  try {
    const result = await createBrandValidate(req.body);
    if (result.length > 0) throw result;
    status = 500;
    const brand = await (await Brands.mergeOn(req.body)).toJson();
    status = 200;
    resp.status(status).json(brand);
  } catch (error) {
    console.log(error);
    resp.status(status).send(
      status === 500
        ? [
            {
              error: true,
              message: "Internal Server Error",
            },
          ]
        : error
    );
  }
};
exports.findAllBrands = async (req, resp, next) => {
  let status = 400;
  try {
    status = 500;
    let query = {};
    if (req.query) {
      query = {
        ...req.query,
      };
    }
    let brand = await (await Brands.all(query)).toJson();
    brand = await Promise.all(
      brand.map(async (el) => await populateBrand(el))
    );
    status = 200;
    resp.status(status).json(brand);
  } catch (error) {
    console.log(error);
    resp.status(status).send(
      status === 500
        ? [
            {
              error: true,
              message: "Internal Server Error",
            },
          ]
        : error
    );
  }
};
