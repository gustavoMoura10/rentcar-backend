const bcrypt = require("bcrypt");
const moment = require("moment");
const { createUserValidate } = require("../validators/usersValidator");
const Users = require("../models/Users");
const Locations = require("../models/Locations");
const Contacts = require("../models/Contacts");
const Images = require("../models/Images");
const { imageKit, bufferImageBase64 } = require("../utils");
const Cars = require("../models/Cars");
const populateUser = async (user) => {
  if (user.location) {
    const location = await (
      await Locations.findById(user.location?._id)
    ).toJson();
    user.location = location;
  }
  if (user.contact) {
    const contact = await (await Contacts.findById(user.contact?._id)).toJson();
    user.contact = contact;
  }
  if (Array.isArray(user.cars) && Array(user.cars).length > 0) {
    user.cars = await Promise.all(
      user.cars.map(async (el) => {
        const car = await (await Cars.findById(el._id)).toJson();
        
      })
    );
  }
  if (Array.isArray(user.carsRented) && Array(user.carsRented).length > 0) {
    user.carsRented = await Promise.all(
      user.carsRented.map(async (el) => {
        el.node = await (await Cars.findById(el.node?._id)).toJson();
        if (el.node?.brand) {
        }
        return el;
      })
    );
  }
  if(user.image){
    const image = await (await Images.findById(user.image?._id)).toJson();
    user.image = image;
  }
  return user;
};
exports.findAllUsers = async (req, resp, next) => {
  let status = 400;
  try {
    status = 500;
    let users = await (await Users.all()).toJson();
    users = await Promise.all(
      users.map(async (el) => {
        delete el.password;
        el = await populateUser(el);
        return {
          ...el,
        };
      })
    );
    status = 200;
    resp.status(status).json(users);
  } catch (error) {
    console.log(error);
    resp.status(status).send(
      status === 500
        ? {
            error: true,
            message: "Internal Server Error",
          }
        : error
    );
  }
};
exports.createUser = async (req, resp, next) => {
  let status = 400;
  try {
    const validate = await createUserValidate(req.body);
    if (validate.length > 0) throw validate;
    status = 500;
    const salt = bcrypt.genSaltSync(10);
    req.body.password = bcrypt.hashSync(req.body.password, salt);
    req.body.birthdate = moment(req.body.birthdate, "YYYY-MM-DD").toDate();
    let user = await Users.mergeOn(req.body);
    if (req.body.image) {
      const buffer = bufferImageBase64(req.body.image);
      const upload = await imageKit.upload({
        file: buffer,
        fileName: `user_image_${new Date().getTime()}.jpg`,
      });
      const image = await Images.mergeOn({
        url: upload.url,
      });
      await user.relateTo(image, "imageOf");
    } else {
      const image = await Images.mergeOn({
        url: "https://ik.imagekit.io/hyxlrlf8obk/_ndice_3W72_8oQj.png",
      });
      await user.relateTo(image, "imageOf");
    }
    user = await user.toJson();
    delete user.password;
    status = 200;
    resp.status(status).json(user);
  } catch (error) {
    console.log(error);
    resp.status(status).send(
      status === 500
        ? [
            {
              error: true,
              message: "Internal Server Error",
            },
          ]
        : error
    );
  }
};
