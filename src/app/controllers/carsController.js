const Brands = require("../models/Brands");
const Cars = require("../models/Cars");
const Users = require("../models/Users");
const Images = require("../models/Images");
const moment = require("moment");
const { bufferImageBase64, imageKit } = require("../utils");
const {
  createCarValidation,
  rentCarValidate,
} = require("../validators/carsValidator");
const Locations = require("../models/Locations");
const populateCar = async (car) => {
  if (Array.isArray(car.images) && Array(car.images).length > 0) {
    car.images = await Promise.all(
      car.images.map(async (el) => {
        return await (await Images.findById(el._id)).toJson();
      })
    );
  }
  if (car.user) {
    car.user = await (await Users.findById(car.user._id)).toJson();
    if (car.user?.location) {
      car.user.location = await (
        await Locations.findById(car.user.location._id)
      ).toJson();
    }
    delete car.user.password;
  }
  if (car.rentedBy) {
    car.rentedBy = await (await Users.findById(car.rentedBy._id)).toJson();
    delete car.rentedBy.password;
  }
  if (car.brand) {
    car.brand = await (await Brands.findById(car.brand._id)).toJson();
  }
  return car;
};
exports.createCar = async (req, resp, next) => {
  let status = 400;
  try {
    const result = await createCarValidation(req.body, req.headers);
    if (result.length > 0) throw result;
    status = 500;
    const images = req.body.images;
    delete req.body.images;
    const car = await Cars.mergeOn(req.body);
    const user = await Users.findById(req.body?.idUser);
    await user.relateTo(car, "forSale");
    const brand = await Brands.findById(req.body?.idBrand);
    await brand.relateTo(car, "manufactored");
    if (Array.isArray(images)) {
      for (let i of images) {
        const buffer = bufferImageBase64(i);
        const upload = await imageKit.upload({
          file: buffer,
          fileName: `car_image_${new Date().getTime()}.jpg`,
        });
        let image = await Images.mergeOn({
          url: upload.url,
        });
        await car.relateTo(image, "imageOf");
      }
    }
    status = 200;
    resp.status(status).json(await car.toJson());
  } catch (error) {
    console.log(error);
    resp.status(status).send(
      status === 500
        ? [
            {
              error: true,
              message: "Internal Server Error",
            },
          ]
        : error
    );
  }
};

exports.findAllCars = async (req, resp, next) => {
  let status = 400;
  try {
    status = 500;
    let query = {};
    if (req.query) {
      query = {
        ...req.query,
      };
    }
    const cars = await Promise.all(
      (await (await Cars.all(query)).toJson()).map((el) => populateCar(el))
    );
    status = 200;
    resp.status(status).json(cars);
  } catch (error) {
    console.log(error);
    resp.status(status).send(
      status === 500
        ? [
            {
              error: true,
              message: "Internal Server Error",
            },
          ]
        : error
    );
  }
};

exports.rentCar = async (req, resp, next) => {
  let status = 400;
  try {
    const result = await rentCarValidate(req.body, req.headers);
    if (result.length > 0) throw result;
    status = 500;
    const car = await Cars.findById(req.body.idCar);
    const user = await Users.findById(req.body.idUser);
    const today = moment();
    const dayReturn = moment().add(req.body.totalDays, "days");
    const rented = {
      totalDays: req.body.totalDays,
      dateRanted: today.toDate(),
      dateReturn: dayReturn.toDate(),
    };
    await car.relateTo(user, "rented", rented);
    await car.update({ isRented: true });
    status = 200;
    const carToJson = await car.toJson();
    return resp.status(status).json(await populateCar(carToJson));
  } catch (error) {
    console.log(error);
    resp.status(status).send(
      status === 500
        ? [
            {
              error: true,
              message: "Internal Server Error",
            },
          ]
        : error
    );
  }
};
