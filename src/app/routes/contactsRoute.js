const { Router } = require("express");
const { authToken } = require("../../config/middlewares");
const { createContact } = require("../controllers/contactsControlle");
const router = Router();

router.post("/createContact", authToken, createContact);

module.exports = router;
