const { Router } = require("express");
const { authToken } = require("../../config/middlewares");
const {
  createBrand,
  findAllBrands,
} = require("../controllers/brandsController");
const router = Router();

router.post("/createBrand", authToken, createBrand);
router.get("/findAllBrands", findAllBrands);

module.exports = router;
