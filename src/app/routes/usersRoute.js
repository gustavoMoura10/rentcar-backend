const { Router } = require("express");
const { authToken } = require("../../config/middlewares");
const { createUser, findAllUsers } = require("../controllers/usersController");
const router = Router();

router.post("/createUser", createUser);
router.get("/findAllUsers", authToken, findAllUsers);

module.exports = router;
