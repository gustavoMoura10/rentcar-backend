const { Router } = require("express");
const { authToken } = require("../../config/middlewares");
const {
  createLocation,
  findLocationById,
  findAllLocations,
} = require("../controllers/locationsController");
const router = Router();

router.post("/createLocation", authToken, createLocation);
router.get("/findLocationById/:_id", authToken, findLocationById);
router.get("/findAllLocations", findAllLocations);

module.exports = router;
