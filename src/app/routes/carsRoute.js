const { Router } = require("express");
const { authToken } = require("../../config/middlewares");
const {
  createCar,
  findAllCars,
  rentCar,
} = require("../controllers/carsController");
const router = Router();

router.post("/createCar", authToken, createCar);
router.get("/findAllCars", findAllCars);
router.put("/rentCar", authToken, rentCar);

module.exports = router;
