const { Router } = require("express");
const { login, newToken } = require("../controllers/authController");
const { authToken } = require('../../config/middlewares')
const router = Router();

router.post("/login", login);
router.get("/newToken", authToken, newToken);

module.exports = router;