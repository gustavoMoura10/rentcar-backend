const { getConnection } = require("../../config/connection");

const Users = getConnection().model("Users", {
  firstName: {
    type: "string",
    required: true,
  },
  lastName: {
    type: "string",
    required: true,
  },
  birthdate: {
    type: "datetime",
    required: true,
  },
  email: {
    type: "string",
    unique: true,
    required: true,
    email: true,
  },
  password: {
    type: "string",
    required: true,
  },
  location: {
    type: "node",
    target: "Locations",
    relationship: "LIVES",
    direction: "out",
    eager: true,
  },
  cars: {
    type: "nodes",
    target: "Cars",
    relationship: "FOR_SALE",
    direction: "out",
    eager: true,
  },
  carsRented: {
    type: "relationships",
    target: "Cars",
    relationship: "RENTED",
    direction: "out",
    eager: true,
    properties: {
      totalDays: "number",
      dateRanted: "datetime",
      dateReturn: "datetime",
    },
  },
  contact: {
    type: "node",
    target: "Contacts",
    relationship: "CONTACTED_BY",
    direction: "out",
    eager: true,
  },
  image: {
    type: "node",
    target: "Images",
    relationship: "IMAGE_OF",
    direction: "out",
    eager: true,
  },
});
Users.relationship(
  "contactedBy",
  "relationship",
  "CONTACTED_BY",
  "out",
  "Contacts"
);
Users.relationship("lives", "relationship", "LIVES", "out", "Locations");
Users.relationship("forSale", "relationship", "FOR_SALE", "out", "Cars");
Users.relationship("rented", "relationship", "RENTED", "out", "Cars", {
  totalDays: "number",
  dateRanted: "datetime",
  dateReturn: "datetime",
});
Users.relationship("imageOf", "relationship", "IMAGE_OF", "out", "Images");
module.exports = Users;
