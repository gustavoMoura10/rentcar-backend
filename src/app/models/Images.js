const { getConnection } = require("../../config/connection");
const Images = getConnection().model("Images", {
  url: {
    type: "string",
    required: true,
  },
  user: {
    type: "node",
    target: "Users",
    relationship: "IMAGE_OF",
    direction: "in",
    eager: true,
  },
  car: {
    type: "node",
    target: "Cars",
    relationship: "IMAGE_OF",
    direction: "in",
    eager: true,
  },
});
Images.relationship("imageOf", "relationship", "IMAGE_OF", "in", "Users");
Images.relationship("imageOf", "relationship", "IMAGE_OF", "in", "Cars");
module.exports = Images;
