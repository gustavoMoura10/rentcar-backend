const { getConnection } = require("../../config/connection");
const Contacts = getConnection().model("Contacts", {
  phoneNumber: {
    type: "string",
    required: true,
    min: 6,
    max: 20,
  },
  cellphoneNumber: {
    type: "string",
    required: true,
    min: 6,
    max: 20,
  },
  contactNumber: {
    type: "string",
    required: true,
    min: 6,
    max: 20,
  },
  user: {
    type: "node",
    target: "Users",
    relationship: "CONTACTED_BY",
    direction: "in",
    eager: true,
  },
});
Contacts.relationship(
  "contactedBy",
  "relationship",
  "CONTACTED_BY",
  "in",
  "Users"
);
module.exports = Contacts;
