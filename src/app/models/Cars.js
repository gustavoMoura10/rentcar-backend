const { getConnection } = require("../../config/connection");
const Cars = getConnection().model("Cars", {
  model: {
    type: "string",
    required: true,
    min: 2,
    max: 150,
  },
  description: {
    type: "string",
    min: 2,
    max: 500,
  },
  year: {
    type: "int",
    required: true,
  },
  totalPerDay: {
    type: "float",
    required: true,
  },
  isRented: {
    type: "boolean",
    required: true,
    default: false,
  },
  user: {
    type: "node",
    target: "Users",
    relationship: "FOR_SALE",
    direction: "in",
    eager: true,
  },
  rentedBy: {
    type: "node",
    target: "Users",
    relationship: "RENTED",
    direction: "in",
    eager: true,
  },
  brand: {
    type: "node",
    target: "Brands",
    relationship: "MANUFACTORED",
    direction: "in",
    eager: true,
  },
  images: {
    type: "nodes",
    target: "Images",
    relationship: "IMAGE_OF",
    direction: "out",
    eager: true,
  },
});
Cars.relationship("forSale", "relationship", "FOR_SALE", "in", "Users");
Cars.relationship("rented", "relationship", "RENTED", "in", "Cars", {
  totalDays: "number",
  dateRanted: "datetime",
  dateReturn: "datetime",
});
Cars.relationship(
  "manufactored",
  "relationship",
  "MANUFACTORED",
  "in",
  "Brands"
);
Cars.relationship("imageOf", "relationship", "IMAGE_OF", "out", "Images");
module.exports = Cars;
