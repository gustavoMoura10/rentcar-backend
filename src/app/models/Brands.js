const { getConnection } = require("../../config/connection");
const Brands = getConnection().model("Brands", {
  name: {
    type: "string",
    required: true,
    min: 2,
    max: 150,
  },
  cars: {
    type: "nodes",
    target: "Cars",
    relationship: "MANUFACTORED",
    direction: "out",
    eager: true,
  },
});
Brands.relationship(
  "manufactored",
  "relationship",
  "MANUFACTORED",
  "out",
  "Brands"
);
module.exports = Brands;
