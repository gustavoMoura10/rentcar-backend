const { getConnection } = require("../../config/connection");
const Locations = getConnection().model("Locations", {
  city: {
    type: "string",
    required: true,
    min: 2,
    max: 150,
  },
  state: {
    type: "string",
    required: true,
    min: 2,
    max: 4,
    uppercase: true,
  },
  country: {
    type: "string",
    required: true,
    min: 2,
    max: 4,
    uppercase: true,
  },
  street: {
    type: "string",
    min: 2,
    max: 150,
    required: true,
  },
  number: {
    type: "int",
    required: true,
  },
  zipCode: {
    type: "string",
    required: true,
  },
  complement: {
    type: "string",
    min: 2,
    max: 150,
    required: true,
  },
  neighborhood: {
    type: "string",
    min: 2,
    max: 150,
    required: true,
  },
  user: {
    type: "node",
    target: "Users",
    relationship: "LIVES",
    direction: "in",
    eager: true,
  },
});
Locations.relationship("lives", "relationship", "LIVES", "in", "Users");
module.exports = Locations;
