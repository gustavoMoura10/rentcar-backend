const { DateTime } = require("neo4j-driver").types;
const { getConnection } = require("../../config/connection");

exports.findAllUsersQuery = () => {
  return getConnection().run(`
        MATCH(n:Users)
        RETURN n;
    `);
};
exports.findUserRelationsByIdQuery = (id) => {
  return getConnection().run(
    `
        MATCH(n:Users)-[r]->(d) 
        WHERE ID(n) = $id
        RETURN d;
    `,
    { id }
  );
};
exports.findUserByEmailQuery = (email) => {
  return getConnection().run(
    `
        MATCH(n:Users{email:$email})
        RETURN n;
    `,
    { email }
  );
};
exports.findUserByIdQuery = (id) => {
  return getConnection().run(
    `
        MATCH(n:Users)
        WHERE ID(n) = $id
        RETURN n;
    `,
    { id }
  );
};

exports.createUserQuery = ({
  email,
  password,
  firstName,
  lastName,
  birthdate,
}) => {
  return getConnection().run(
    `
        CREATE(n:Users{email:$email,password:$password,firstName:$firstName,lastName:$lastName,birthdate:$birthdate})
        RETURN n;
    `,
    {
      email,
      password,
      firstName,
      lastName,
      birthdate: DateTime.fromStandardDate(birthdate),
    }
  );
};
