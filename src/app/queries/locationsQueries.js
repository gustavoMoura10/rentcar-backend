const { getConnection } = require("../../config/connection");

exports.createLocationQuery = ({
  city,
  state,
  country,
  street,
  number,
  complement,
  neighborhood,
  zipCode,
  idUser,
}) => {
  return getConnection().run(
    `
        MATCH (u:Users) WHERE ID(u) = $idUser
        MERGE (l:Locations {
            city:$city,
            state:$state,
            country:$country,
            street:$street,
            number:$number,
            complement:$complement,
            neighborhood:$neighborhood,
            zipCode:$zipCode
        })
        MERGE (u)-[li:LIVES]->(l)
        RETURN l
    `,
    {
      city,
      state,
      country,
      street,
      number,
      complement,
      neighborhood,
      zipCode,
      idUser,
    }
  );
};

exports.findLocationByUserIdQuery = (idUser) => {
  return getConnection().run(
    `
    MATCH(u:Users) WHERE ID(u) = $idUser
    MATCH(u)-[:LIVES]->(l)
    RETURN l;
  `,
    { idUser }
  );
};
