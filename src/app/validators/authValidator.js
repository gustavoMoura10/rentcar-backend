const Joi = require("joi");
const Users = require("../models/Users");
exports.loginValidate = async (body) => {
  let errors = [];
  const schema = Joi.object({
    email: Joi.string().email().required(),
    password: Joi.string().min(6).max(15).required(),
  });
  let result = schema.validate(body);
  if (result.error) {
    errors = [
      ...errors,
      ...result.error.details.map((el) => {
        return {
          ...el,
          error: true,
        };
      }),
    ];
  }
  const user = await Users.first({ email: body.email });
  if (!user) {
    errors.push({
      error: true,
      message: "User with that email not found",
    });
  }
  return errors;
};
