const Joi = require("joi");
const Cars = require("../models/Cars");
const Users = require("../models/Users");

exports.createCarValidation = async (body, headers) => {
  let errors = [];
  const schema = Joi.object({
    model: Joi.string().min(2).max(150).required(),
    description: Joi.string().min(2).max(500),
    year: Joi.number().integer().required(),
    totalPerDay: Joi.number().required(),
    images: Joi.array(),
    idUser: Joi.number().integer().required(),
    idBrand: Joi.number().integer().required(),
  });
  let result = schema.validate(body);
  if (result.error) {
    errors = [
      ...errors,
      ...result.error.details.map((el) => {
        return {
          ...el,
          error: true,
        };
      }),
    ];
  }
  let user = await Users.findById(body.idUser);
  if (!user) {
    errors.push({
      error: true,
      message: "User id not found",
    });
  }
  if (user) {
    const userToJson = await user.toJson();
    if (userToJson._id !== headers._id) {
      errors.push({
        error: true,
        message: "User not allowed",
      });
    }
  }
  return errors;
};

exports.rentCarValidate = async (body, headers) => {
  let errors = [];
  const schema = Joi.object({
    idUser: Joi.number().required(),
    idCar: Joi.number().required(),
    totalDays: Joi.number().required(),
  });
  let result = schema.validate(body);
  if (result.error) {
    errors = [
      ...errors,
      ...result.error.details.map((el) => {
        return {
          ...el,
          error: true,
        };
      }),
    ];
  }
  let user = await Users.findById(body.idUser);
  if (!user) {
    errors.push({
      error: true,
      message: "User id not found",
    });
  }

  let car = await Cars.findById(body.idCar);
  if (!car) {
    errors.push({
      error: true,
      message: "Car id not found",
    });
  }
  if (car) {
    const carToJson = await car.toJson();
    if (carToJson.isRented) {
      errors.push({
        error: true,
        message: "Car not available yet",
      });
    }
    const userToJson = await user.toJson();
    if (userToJson._id === carToJson.user?._id) {
      errors.push({
        error: true,
        message: "User not allowed",
      });
    }
  }
  return errors;
};
