const Joi = require("joi");
exports.createBrandValidate = async (body) => {
  let errors = [];
  const schema = Joi.object({
    name: Joi.string().min(2).max(150).required(),
  });
  let result = schema.validate(body);
  if (result.error) {
    errors = [
      ...errors,
      ...result.error.details.map((el) => {
        return {
          ...el,
          error: true,
        };
      }),
    ];
  }

  return errors;
};
