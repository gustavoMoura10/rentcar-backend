const Joi = require("joi");
const Users = require("../models/Users");
//const { findUserByEmailQuery } = require("../queries/usersQueries");
exports.createUserValidate = async (body) => {
  let errors = [];
  const schema = Joi.object({
    email: Joi.string().email().required(),
    password: Joi.string().min(6).max(15).required(),
    confirmPassword: Joi.string()
      .min(6)
      .max(15)
      .valid(Joi.ref("password"))
      .required(),
    firstName: Joi.string().alphanum().min(2).max(150).required(),
    lastName: Joi.string().alphanum().min(2).max(150).required(),
    birthdate: Joi.date().required(),
  });
  let result = schema.validate(body);
  if (result.error) {
    errors = [
      ...errors,
      ...result.error.details.map((el) => {
        return {
          ...el,
          error: true,
        };
      }),
    ];
  }
  const user = await Users.first({ email: body.email });
  if (user) {
    errors.push({
      error: true,
      message: "Email already exists",
    });
  }
  
  return errors;
};
