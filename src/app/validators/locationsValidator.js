const Joi = require("joi");
const Locations = require("../models/Locations");
const Users = require("../models/Users");

exports.createLocationValidate = async (body, headers) => {
  let errors = [];
  const schema = Joi.object({
    city: Joi.string().min(2).max(150).required(),
    state: Joi.string().min(2).max(4).required(),
    country: Joi.string().min(2).max(4).required(),
    street: Joi.string().min(2).max(150).required(),
    number: Joi.number().required(),
    zipCode: Joi.string().required(),
    complement: Joi.string().min(2).max(150).required(),
    neighborhood: Joi.string().min(2).max(150).required(),
    idUser: Joi.number().required(),
  });
  let result = schema.validate(body);
  if (result.error) {
    errors = [
      ...errors,
      ...result.error.details.map((el) => {
        return {
          ...el,
          error: true,
        };
      }),
    ];
  }
  let user = await Users.findById(body.idUser);
  if (!user) {
    errors.push({
      error: true,
      message: "User id not found",
    });
  }
  if (user && user.get("location")) {
    errors.push({
      error: true,
      message: "User already have a location",
    });
  }
  if (user) {
    const userToJson = await user.toJson();
    if (userToJson._id !== headers._id) {
      errors.push({
        error: true,
        message: "User not allowed",
      });
    }
  }
  return errors;
};

exports.findLocationByIdValidate = async (params) => {
  let errors = [];
  if (!params._id) {
    errors.push({
      error: true,
      message: "Missing ID",
    });
  }
  if (params._id) {
    const location = await Locations.findById(params._id);
    if (!location) {
      errors.push({
        error: true,
        message: "Location not found",
      });
    }
  }
  return errors;
};
