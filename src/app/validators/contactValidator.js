const Joi = require("joi");
const Users = require("../models/Users");
exports.createContactValidate = async (body, headers) => {
  let errors = [];
  const schema = Joi.object({
    phoneNumber: Joi.string().min(6).max(20).required(),
    cellphoneNumber: Joi.string().min(6).max(20).required(),
    contactNumber: Joi.string().min(6).max(20).required(),
    idUser: Joi.number().required(),
  });
  let result = schema.validate(body);
  if (result.error) {
    errors = [
      ...errors,
      ...result.error.details.map((el) => {
        return {
          ...el,
          error: true,
        };
      }),
    ];
  }
  let user = await Users.findById(body.idUser);
  if (!user) {
    errors.push({
      error: true,
      message: "User id not found",
    });
  }
  if (user && user.get("contact")) {
    errors.push({
      error: true,
      message: "User already have a contact",
    });
  }
  if (user) {
    const userToJson = await user.toJson();
    if (userToJson._id !== headers._id) {
      errors.push({
        error: true,
        message: "User not allowed",
      });
    }
  }
  return errors;
};
